import readline from "readline";
import https from "https";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("What is your name? ", function (name) {
  rl.question(`Hi, ${name}, wanna hear some jokes? [Y/N] `, function (answer) {
    if (answer === "Y") {
      const request = https.request(
        "https://api.chucknorris.io/jokes/categories",
        function (response) {
          response.on("data", function (data) {
            const categories = JSON.parse(data.toString());

            categories.forEach(function (c, i) {
              console.log(`${i + 1}. ${c}`);
            });

            rl.question(
              "Please, select a category (Enter number): ",
              function (number) {
                const index = number - 1;
                const selectedCategory = categories[index];

                const request = https.request(
                  `https://api.chucknorris.io/jokes/random?category=${selectedCategory}`,
                  function (response) {
                    response.on("data", function (data) {
                      const joke = JSON.parse(data.toString());
                      console.log(joke.value);
                      rl.close();
                    });
                  }
                );

                request.end();
              }
            );
          });
        }
      );
      request.end();
    } else {
      console.log("Get the hell out of here!");
      rl.close();
    }
  });
});
