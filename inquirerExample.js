import inquirer from "inquirer";

await inquirer.prompt({
  type: "input",
  name: "username",
  message: "What is your name?",
  default() {
    return "Unknown";
  },
});

await inquirer.prompt({
  type: "list",
  name: "question",
  message: "What is your favorite movie character?",
  choices: [
    "Polat Alemdar",
    "Thomas Shelby",
    "Steve Rogers",
    "William Butcher",
  ],
});
