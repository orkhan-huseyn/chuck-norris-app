import https from "https";

export function request(url) {
  return new Promise(function (resolve, reject) {
    const req = https.request(url, function (response) {
      let body = [];

      response.on("data", function (data) {
        body.push(data);
      });

      response.on("end", function () {
        body = Buffer.concat(body).toString();
        resolve(JSON.parse(body));
      });
    });
    req.on("error", reject);
    req.end();
  });
}
