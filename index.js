import { question, rl } from "./question.js";
import { request } from "./request.js";

const name = await question("What is your name? ");
const answer = await question(`Hi, ${name}, wanna hear some jokes? [Y/N] `);

if (answer === "Y") {
  const categories = await request(
    "https://api.chucknorris.io/jokes/categories"
  );
  categories.forEach(function (c, i) {
    console.log(`${i + 1}. ${c}`);
  });

  const number = await question("Please, select a category (Enter number): ");
  const index = number - 1;
  const selectedCategory = categories[index];

  const joke = await request(
    `https://api.chucknorris.io/jokes/random?category=${selectedCategory}`
  );
  console.log(joke.value);
  rl.close();
} else {
  console.log("Get the hell out of here!");
  rl.close();
}
